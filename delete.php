
<?php

//including the database crud file
include_once "dist/includes/crud.php";

//Creating CRUD object

$crud = new Crud();

//Getting data and convert
$requestPayload = json_decode(file_get_contents("php://input"));

//set variable with msg and type
$alert_data = ["msg" => "Succesfully deleted", "type" => "success"];

//delete data from DB
$result = $crud->delete(implode(",", $requestPayload), 'products');

//if data was deleted succesfully send msg and type
if ($result) {
    echo json_encode($alert_data);
}
;
