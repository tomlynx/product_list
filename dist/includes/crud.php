<?php
include_once 'db_connect.php';
class Crud extends DbConfig
{
    public function __construct()
    {
        parent::__construct();
    }
    //Get data from DB
    public function getData($query)
    {
        $result = $this->connection->query($query);

        if ($result == false) {
            return false;
        }

        $rows = array();

        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    //Save to DB

    public function saveToDB($product, $alert_data_success, $alert_data_fail)
    {

        $keys = [];
        $values = [];

        foreach ($product as $key => $value) {
            array_push($values, $value);
            array_push($keys, $key);
        }

        $keys = implode(",", $keys);
        $values = "'" . implode("','", $values) . "'";
        $result = $this->connection->query("INSERT INTO products($keys)
        VALUES($values)");

        if ($result) {
            echo json_encode($alert_data_success);
        } else {
            echo json_encode($alert_data_fail);
        }

    }

    //Delete data  from DB using id
    public function delete($id, $table)
    {
        $query = "DELETE FROM $table WHERE id IN ($id)";

        $result = $this->connection->query($query);

        if ($result == false) {
            echo 'Error: cannot delete ID' . $id . ' from table ' . $table;
            return false;
        } else {
            return true;
        }
    }

    //Delete all data from DB.
    public function delete_all($table)
    {
        $query = "DELETE FROM $table";

        $result = $this->connection->query($query);

        if ($result == false) {
            echo 'Error: cannot delete ' . $table;
            return false;
        } else {
            return true;
        }
    }
}
