<?php

//Product abstract class

abstract class Product
{
    public $sku;
    public $name;
    public $price;

    public function __construct($requestPayload)
    {
        $this->sku = $requestPayload->SKU;
        $this->name = $requestPayload->name;
        $this->price = (int) $requestPayload->price;
    }

}

class CD extends Product
{
    public $size;
    public function __construct($requestPayload)
    {
        parent::__construct($requestPayload);
        $this->size = $requestPayload->size . "MB";

    }

}

class Book extends Product
{
    public $weight;
    public function __construct($requestPayload)
    {
        parent::__construct($requestPayload);
        $this->weight = $requestPayload->weight . "KG";

    }

}

class Furniture extends Product
{
    public $dimensions;

    public function __construct($requestPayload)
    {
        parent::__construct($requestPayload);
        $this->dimensions = $requestPayload->height . "x" . $requestPayload->width . "x" . $requestPayload->length;

    }

}

class Factory
{
    public function create($requestPayload)
    {

        switch ($requestPayload->type) {
            case "cd":
                return new CD($requestPayload);
                break;
            case "furniture":
                return new Furniture($requestPayload);
                break;
            case "book":
                return new Book($requestPayload);
                break;
        }
    }
}
