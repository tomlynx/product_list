//Variables
const delete_form = document.getElementById("delete_all");
const checkbox = document.querySelectorAll(".delete_checkbox");
const alert_msg = document.getElementById("alert");
const add_div = document.querySelector("#product_list");
const delete_action = document.getElementById("actions");

delete_form.onsubmit = function () {
  //Check if any checkbox is checked
  function anyCheckbox() {
    for (var i = 0; i < checkbox.length; i++)
      if (checkbox[i].checked) return true;
    return false;
  }
  const check = anyCheckbox();

  //if not one checkbox is checked set alert msg
  if (!check && delete_action.value != "delete_all") {
    showAlert("Choose product to delete", "danger");
  } else if (check && delete_action.value != "delete_all") {
    //make array of all checkboxes which are checked
    let checked = Array.from(checkbox)
      .filter((item) => {
        return item.checked == true;
      })
      .map((item) => {
        return item.value;
      });

    //delete item or muplitple items fetch function
    const delete_items = async (values) => {
      const response = await fetch("delete.php", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },

        body: JSON.stringify(values),
      });
      return await response.json();
    };

    //call delete item function, pass array with checkbox values which are checked
    delete_items(checked)
      .then((data) => showAlert(data.msg, data.type))
      .catch((err) => console.log(err));
  } else {
    //Delele all items fetch function
    const delete_all = async () => {
      let response = await fetch("delete_all.php");

      let data = await response.json();

      return data;
    };

    //call delete all function
    delete_all()
      .then((data) => showAlert(data.msg, data.type))
      .catch((err) => console.log(err));
  }
};

//Alert function
function showAlert(msg, type) {
  //Set msg and type to localstorage
  localStorage.setItem("msg", msg);
  localStorage.setItem("type", type);
}

window.onload = function () {
  //display alert if localstorage item is setted
  if (localStorage.getItem("type")) {
    const alert_msg = document.createElement("span");
    alert_msg.classList.add("alert", localStorage.getItem("type"));
    alert_msg.innerHTML = localStorage.getItem("msg");
    add_div.appendChild(alert_msg);
    setTimeout(function () {
      alert_msg.remove();
    }, 3000);
    //Clear localstorage
    localStorage.clear();
  }
};
