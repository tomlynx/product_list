//Variables

const add_form = document.getElementById("add_product_db");
const add_div = document.querySelector(".add_product");

//add product fetch function
const add_product = async (data) => {
  const response = await fetch("add_product.php", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },

    body: JSON.stringify(data),
  });
  return await response.json();
};

add_form.onsubmit = function (e) {
  //Get data from form
  function get_data_from_form() {
    let elements = add_form.elements;
    var form_obj = {};
    for (var i = 0; i < elements.length; i++) {
      var item = elements.item(i);
      form_obj[item.name] = item.value;
    }

    //according to product type make validation
    switch (form_obj.type) {
      case "cd":
        //check if all fields are filled, if not show alert
        if (
          form_obj.SKU.length <= 0 ||
          form_obj.name.length <= 0 ||
          form_obj.price.length <= 0 ||
          form_obj.size.length <= 0
        ) {
          showAlert("Please fill all fields", "danger");
        }
        //Check if price and size is not setted negative
        else if (form_obj.price <= 0 || form_obj.size <= 0) {
          showAlert("Price or Size cannot be negative number", "danger");
        } else {
          add_product(form_obj)
            .then((alert) => showAlert(alert.msg, alert.type))
            .catch((error) => console.log(error));
        }
        break;
      case "book":
        if (
          form_obj.SKU.length <= 0 ||
          form_obj.name.length <= 0 ||
          form_obj.price.length <= 0 ||
          form_obj.weight.length <= 0
        ) {
          showAlert("Please fill all fields", "danger");
        }
        //Check if price and size is not setted negative
        else if (form_obj.price <= 0 || form_obj.weight <= 0) {
          showAlert("Price or Weight cannot be negative number", "danger");
        } else {
          add_product(form_obj)
            .then((alert) => showAlert(alert.msg, alert.type))
            .catch((err) => console.log(err));
        }
        break;
      case "furniture":
        if (
          form_obj.SKU.length <= 0 ||
          form_obj.name.length <= 0 ||
          form_obj.price.length <= 0 ||
          form_obj.height.length <= 0 ||
          form_obj.width.length <= 0 ||
          form_obj.length.length <= 0
        ) {
          showAlert("Please fill all fields", "danger");
        }
        //Check if price and dimensions  is not setted negative
        else if (
          form_obj.price <= 0 ||
          form_obj.height <= 0 ||
          form_obj.width <= 0 ||
          form_obj.length <= 0
        ) {
          showAlert("Price and Dimensions cannot be negative number", "danger");
        } else {
          add_product(form_obj)
            .then((alert) => showAlert(alert.msg, alert.type))
            .catch((err) => console.log(err));
        }
        break;
    }
  }
  get_data_from_form();
};

//Alert function
function showAlert(msg, type) {
  //Set msg and type to localstorage
  localStorage.setItem("msg", msg);
  localStorage.setItem("type", type);
}

window.onload = function () {
  //display alert if localstorage item is setted
  if (localStorage.getItem("type")) {
    const alert_msg = document.createElement("span");
    alert_msg.classList.add("alert", localStorage.getItem("type"));
    alert_msg.innerHTML = localStorage.getItem("msg");
    add_div.appendChild(alert_msg);
    setTimeout(function () {
      alert_msg.remove();
    }, 3000);
    //Clear localstorage
    localStorage.clear();
  }
};
