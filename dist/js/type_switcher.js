//Variables

const book_card = document.getElementById("book_type");
const cd_card = document.getElementById("cd_type");
const furniture_card = document.getElementById("furniture_type");
const type_switcher = document.getElementById("type_switcher");

//Add event listener to dynamicaly change input of different product

type_switcher.addEventListener("change", function () {
  switch (type_switcher.value) {
    case "book":
      book_card.style.display = "block";
      cd_card.style.display = "none";
      furniture_card.style.display = "none";
      break;
    case "furniture":
      book_card.style.display = "none";
      cd_card.style.display = "none";
      furniture_card.style.display = "block";
      break;
    case "cd":
      book_card.style.display = "none";
      cd_card.style.display = "block";
      furniture_card.style.display = "none";
      break;
  }
});
