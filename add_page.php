<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="dist/css/main.css">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
  <title>Product Add</title>
</head>
<body>
<!-- Header -->
<header class="header_main bg-dark">

  <h1 class="py-2">Product Add</h1>
  <a href="index.php" class="btn">Show Products</a>
  <button type="submit" form="add_product_db" class="btn">Save</button>
  </header>

  <!-- Product add form -->
    <div class="add_product">
        <div class="container">
<!-- Product form  -->
        <form id="add_product_db">
            <div class="input_group">
            <label for="SKU">SKU</label>
            <input type="text" placeholder="SKU" name="SKU" id="sku">
            </div>
            <div class="input_group">
            <label for="Name">Name</label>
            <input type="text" placeholder="Name" name="name" id="name" >
            </div>
            <div class="input_group">
            <label for="Price">Price</label>
            <input type="number" placeholder="Price" name="price" id="price" >
            </div>
            <div class="input_group">
            <span>Type</span>
            <select name="type" id="type_switcher" >
            <option value="cd">CD</option>
            <option value="book">Book</option>
            <option value="furniture" >Furniture</option>
            </select>
            </div>
            <div class="input_group type_form">


            <!-- CD type product -->
            <div id="cd_type">
                <div class="input_group">
                    <label for="size">Size</label>
                    <input type="number" placeholder="Size" name="size"  id="size">
                </div>
                <p>Please provide storage capacity in MB.</p>
            </div>
            <!-- Book type product -->
            <div id="book_type">
                <div class="input_group">
                    <label for="weight">Weight </label>
                    <input type="number" placeholder="Weight" name="weight" id="weight">
                </div>
                <p>Please provide weight in kilograms.</p>
            </div>
            <!-- Furniture type product -->
            <div id="furniture_type">
                <div class="input_group">
                    <label for="height">Height</label>
                    <input type="number" placeholder="Height" name="height" id="height" >
                </div>
                <div class="input_group">
                    <label for="width">Width</label>
                    <input type="number" placeholder="Width" name="width" id="width">
                </div>
                <div class="input_group">
                    <label for="length">Length</label>
                    <input type="number" placeholder="Length" name="length" id="length">
                </div>
                <p>Please provide dimensions in HxWxLx format.</p>
            </div>
            </div>

        </form>
        </div>
    </div>



    <script src="dist/js/type_switcher.js"></script>
    <script src="dist/js/validation_add.js"></script>
</body>
</html>
