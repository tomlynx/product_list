<?php
//including the database connection file
include_once "dist/includes/crud.php";

$crud = new Crud();

//fetching data from DB
$query = "SELECT * FROM `products`";
$result = $crud->getData($query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="dist/css/main.css">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
  <title>Product List</title>
</head>
<body>
<!-- Header -->
 <header class="header_main bg-dark">

 <h1 class="py-2">Product List</h1>
 <a href="add_page.php" class="btn">Add Product</a>
 <div class="delete_action">
 <select name="actions" id="actions" >
  <option value="delete">Delete</option>
  <option value="delete_all">Delete All</option>
</select>
<button type="submit" form="delete_all" class="btn">Apply</button>
</div>

 </header>

 <!-- Product List -->
 <div id="product_list" class="bg-medium">
  <div class="container">
  <div class="card_container">
  <form id="delete_all">
  <?php

//  Loop response from DB and output as card item

foreach ($result as $res) {
    ?>
   <div class="card_item" id="<?php echo $res["id"] ?>">

   <input type="checkbox"  name="delete[]"  value="<?php echo $res["id"] ?>"class="delete_checkbox">
   <span class="card_span"> <?php echo $res["sku"] ?></span>
   <span class="card_span"><?php echo $res["name"] ?></span>
   <span class="card_span"><?php echo $res["price"] ?> $</span>

  <?php
if (!empty($res["size"])):
    ?> <span class="card_span">Size:<?php echo $res["size"] ?></span><?php
elseif (!empty($res["weight"])):
    ?><span class="card_span">Weight:<?php echo $res["weight"] ?></span><?php
else:
    ?><span class="card_span">Dimensions:<?php echo $res["dimensions"] ?></span>

  <?php endif;?>

     </div>

   <?php
}
?>
  </form>
  </div>
  </div>
 </div>
 <script src="dist/js/validation_delete.js"></script>
</body>
</html>
