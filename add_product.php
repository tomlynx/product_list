<?php
include_once "dist/includes/crud.php";
include_once "dist/includes/products_classes.php";

//Creating CRUD object
$crud = new Crud();
//creating factory object

$factory = new Factory();

$requestPayload = json_decode(file_get_contents("php://input"));
$sku = $requestPayload->sku;

//Alert messages

$alert_data = ["msg" => "The Product was succesfully added", "type" => "success"];
$alert_data_danger = ["msg" => "SKU number has to be unique", "type" => "danger"];
$alert_data_fail = ["msg" => "Oopss, Something went wrong :(", "type" => "danger"];

//check if SKU is unique
$query = "SELECT * FROM products where sku = '" . $sku . "' ";
$sku_check = $crud->getData($query);

if (count($sku_check) > 0) {
    echo json_encode($alert_data_danger);
} else {
    //Create product and save in DB
    $product = $factory->create($requestPayload);

    $crud->saveToDB($product, $alert_data, $alert_data_fail);

}
