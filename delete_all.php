<?php

//including the database crud file
include_once("dist/includes/crud.php");

//Creating CRUD object

$crud = new Crud();

//set variable with msg and type
$alert_data = ["msg"=>"All Products was succesfully deleted","type"=>"success"];

$result =$crud->delete_all('products');

//if data was deleted succesfully send msg and type
if ($result) {
    echo json_encode($alert_data);
};
